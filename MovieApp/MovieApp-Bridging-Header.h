//
//  MovieApp-Bridging-Header.h
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

#ifndef MovieApp_Bridging_Header_h
#define MovieApp_Bridging_Header_h
#import <UIKit/UIKit.h>
#import <UIScrollView_InfiniteScroll/UIScrollView+InfiniteScroll.h>

#endif /* MovieApp_Bridging_Header_h */
