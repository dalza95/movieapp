//
//  ViewController.swift
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MoviesController: UIViewController, UICollectionViewDelegate, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let refreshControl = UIRefreshControl()
    @IBOutlet weak var cvMovies: UICollectionView!
    @IBOutlet weak var sbSearch: UISearchBar!
    
    //vars
    var movies = [Movie]()
    var dictionaryHeight = [String: CGSize]()
    
    var showContent = false
    var errorService = false
    var msmService = ""
    var pagination = 1
    var alamofireManager: Alamofire.SessionManager!
    
    //------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareController()
    }

    func prepareController(){
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 20
        self.alamofireManager = Alamofire.SessionManager(configuration: configuration)
        
        self.setupCollectionMovies()
        self.callWsMovies()
    }
    
    func setupCollectionMovies(){
        let loadingCell = UINib(nibName: "LoadingViewCell", bundle: nil)
        self.cvMovies.register(loadingCell, forCellWithReuseIdentifier: "LoadingViewCell")
        
        let msmServiceCell = UINib(nibName: "MsmServiceViewCell", bundle: nil)
        self.cvMovies.register(msmServiceCell, forCellWithReuseIdentifier: "MsmServiceViewCell")
        
        let noDataCell = UINib(nibName: "NoDataViewCell", bundle: nil)
        self.cvMovies.register(noDataCell, forCellWithReuseIdentifier: "NoDataViewCell")
        
        let movieCell = UINib(nibName: "MovieViewCell", bundle: nil)
        self.cvMovies.register(movieCell, forCellWithReuseIdentifier: "MovieViewCell")
        
        self.refreshControl.tintColor = UIColor.gray
        self.refreshControl.addTarget(self, action: #selector(pullToRefresh(sender:)), for: .valueChanged)
        self.cvMovies.addSubview(refreshControl)
        self.cvMovies.isScrollEnabled = false
        self.cvMovies.reloadData()
    }
    
    @objc func pullToRefresh(sender: AnyObject){
        self.showContent = false
        self.errorService = false
        self.msmService = ""
        
        self.refreshControl.endRefreshing()
        self.cvMovies.setContentOffset(CGPoint.zero, animated: false)
        self.cvMovies.isScrollEnabled = false
        self.cvMovies.reloadData()
        
        self.pagination = 1
        self.movies.removeAll()
        self.dictionaryHeight.removeAll()
        self.callWsMovies()
    }
    
    func prepareInfinityScrolling(){
        let indicator = UIActivityIndicatorView()
        indicator.style = .white
        indicator.color = UIColor.gray
        self.cvMovies.infiniteScrollIndicatorView = indicator
        self.cvMovies.addInfiniteScroll { (collectionView) -> Void in self.infinityScrolling() }
    }
    
    func infinityScrolling(){
        self.pagination = self.pagination + 1
        self.callWsMovies()
    }
    
    func goMovieDetailController(movieSelected: Movie){
//        let patientDetailController = PatientDetailController()
//        patientDetailController.cPatient = patientSelected
//        self.navigationController!.pushViewController(patientDetailController, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        movies.removeAll()
        callWsMovies()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        movies.removeAll()
        callWsMovies()
    }
    
    //Collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.showContent == false || self.errorService == true || self.movies.count == 0){
            return 1
        }else{
            return self.movies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(self.showContent == false){
            let key = "LoadingViewCell_\(indexPath.section)_\(indexPath.item)"
            let size = self.dictionaryHeight[key]!
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoadingViewCell", for: indexPath) as! LoadingViewCell
            cell.updateSizeCell(widthCollection: size.width, heightCollection: size.height)
            cell.loading.startAnimating()
            return cell
            
        }else{
            if(self.errorService == true){
                let key = "MsmServiceViewCell_\(indexPath.section)_\(indexPath.item)"
                let size = self.dictionaryHeight[key]!
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MsmServiceViewCell", for: indexPath) as! MsmServiceViewCell
                cell.updateSizeCell(widthCollection: size.width, heightCollection: size.height)
                cell.labelMsm.text = self.msmService
                cell.btnTryAgain.addTarget(self, action: #selector(tryAgain(_:)), for: .touchUpInside)
                return cell
                
            }else{
                if(self.movies.count == 0){
                    let key = "NoDataViewCell_\(indexPath.section)_\(indexPath.item)"
                    let size = self.dictionaryHeight[key]!
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataViewCell", for: indexPath) as! NoDataViewCell
                    cell.updateSizeCell(widthCollection: size.width, heightCollection: size.height)
                    
                    return cell
                    
                }else{
                    let key = "MovieViewCell_\(indexPath.section)_\(indexPath.item)"
                    let size = self.dictionaryHeight[key]!
                    
                    let oMovie = self.movies[indexPath.item]
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieViewCell", for: indexPath) as! MovieViewCell
                    cell.updateSizeCell(widthCollection: size.width, heightCollection: 300)
                    cell.setPhoto(urlPhoto: oMovie.urlImage)
                    cell.lblNameMovie.text = oMovie.name + " (\(oMovie.year!))"
                    cell.lblDescription.text = oMovie.overview
                    return cell
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(self.showContent == false) {
            let key = "LoadingViewCell_\(indexPath.section)_\(indexPath.item)"
            if(self.dictionaryHeight.keys.contains(key) == false){
                self.dictionaryHeight[key] = collectionView.bounds.size
            }
            return self.dictionaryHeight[key]!
            
        } else {
            if(self.errorService == true) {
                let key = "MsmServiceViewCell_\(indexPath.section)_\(indexPath.item)"
                if(self.dictionaryHeight.keys.contains(key) == false){
                    self.dictionaryHeight[key] = collectionView.bounds.size
                }
                return self.dictionaryHeight[key]!
                
            }else{
                if(self.movies.count == 0){
                    let key = "NoDataViewCell_\(indexPath.section)_\(indexPath.item)"
                    if(self.dictionaryHeight.keys.contains(key) == false){
                        self.dictionaryHeight[key] = collectionView.bounds.size
                    }
                    return self.dictionaryHeight[key]!
                    
                }else{
                    let key = "MovieViewCell_\(indexPath.section)_\(indexPath.item)"
                    if(self.dictionaryHeight.keys.contains(key) == false){
//                        let cMovie = self.movies[indexPath.item]
                        let cell = Bundle.main.loadNibNamed("MovieViewCell", owner: self, options: nil)![0] as! MovieViewCell
                        cell.updateSizeCell(widthCollection: collectionView.bounds.width, heightCollection: 300)
                        cell.setNeedsLayout()
                        cell.layoutIfNeeded()
                        let size = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
                        self.dictionaryHeight[key] = size
                    }
                    return self.dictionaryHeight[key]!
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if(self.showContent == true && self.errorService == false && self.movies.count > 0){
            let edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return edgeInsets
        }else{
            return UIEdgeInsets.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let spacingForItem = CGFloat(5)
        return spacingForItem
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.movies.count > 0){
            let movieSelected = self.movies[indexPath.item]
            self.goMovieDetailController(movieSelected: movieSelected)
        }
        
    }
    
    
    //Button Action
    @IBAction func tryAgain(_ sender: UIButton) {
        self.pullToRefresh(sender: self.refreshControl)
    }
    
    
    //Fuction WS
    func callWsMovies(){
        let dataToSendHeader: HTTPHeaders = ["Content-Type": "application/json", "trakt-api-key":GlobalMovie.api_secretKey, "trakt-api-version":"2"]
        
        let search =  self.sbSearch.text!.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        let urlWs = GlobalMovie.getMovies(query: search, page: self.pagination, limit: 10)
        self.alamofireManager.request(urlWs, method:.get, headers: dataToSendHeader).responseJSON{ response in
            switch response.result {
            case.success(let jsonResult):
                print(jsonResult)
                
                let responseWS = JSON(jsonResult).arrayValue
                
                if(responseWS.count == 0){
                    self.cvMovies.removeInfiniteScroll()
                }else{
                    
                    self.prepareInfinityScrolling()
                    
                    for movie in responseWS {
                        
                        let ids = movie["ids"]
                        let id = ids["tmdb"].intValue
                        let title = movie["title"].stringValue
                        let year = movie["year"].intValue
                        let overview = movie["overview"].stringValue
                        
                        self.callWsGetImage(idImage: id, completion: {(response) in
                            
                            let data = JSON(response)
                            let path_image = data["backdrop_path"].stringValue
                            let path = "https://image.tmdb.org/t/p/w500/" + path_image
                            
                            
                            let oMovie = Movie.init(id: id, name: title, year: year, overview: overview, urlImage: path)
                            
                            self.movies.append(oMovie)
                            self.cvMovies.reloadData()
                            
                        })
                        
                        
                    }
                    
                }
                
            case .failure(let Error):
                if(self.pagination == 1){
                    self.errorService = true
                    if(Error._code == NSURLErrorTimedOut || Error._code == NSURLErrorNotConnectedToInternet || Error._code == NSURLErrorNetworkConnectionLost){
                        self.msmService = "Verifique su conexión a internet"
                    }else{
                        self.msmService = "Ocurrió un problema al mostrar el contenido"
                    }
                }
            }
            
            self.showContent = true
            self.cvMovies.finishInfiniteScroll()
            self.cvMovies.isScrollEnabled = true
            self.cvMovies.reloadData()
        }
    }
    
    func callWsGetImage(idImage: Int, completion: @escaping ((_ response: JSON) -> Void)){
        
        let dataToSendHeader: HTTPHeaders = ["Content-Type": "application/json"]
        
        let urlWs = GlobalMovie.getUrlImageFromTheMovieDB(idImage: idImage)
        self.alamofireManager!.request(urlWs, method: .get, headers: dataToSendHeader).responseJSON{
            response in
            switch response.result {
            case.success(let jsonResult):
                completion(JSON(jsonResult))
            case.failure(let Error):
                break
            }
            
        }
        
    }
    
}

