//
//  NoDataViewCell.swift
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

import UIKit

class NoDataViewCell: UICollectionViewCell {

    @IBOutlet weak var cnWidthCell: NSLayoutConstraint!
    @IBOutlet weak var cnHeightCell: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateSizeCell(widthCollection: CGFloat, heightCollection: CGFloat) {
        self.cnWidthCell.constant = widthCollection
        self.cnHeightCell.constant = heightCollection
    }
    
}
