//
//  LoadingViewCell.swift
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

import UIKit

class LoadingViewCell: UICollectionViewCell {

    @IBOutlet weak var cnHeightCell: NSLayoutConstraint!
    @IBOutlet weak var cnWidthCell: NSLayoutConstraint!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateSizeCell(widthCollection: CGFloat, heightCollection: CGFloat){
        self.cnWidthCell.constant = CGFloat(Int(widthCollection))
        self.cnHeightCell.constant = CGFloat(Int(heightCollection))
    }
    
}
