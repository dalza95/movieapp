//
//  MovieViewCell.swift
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieViewCell: UICollectionViewCell {

    @IBOutlet weak var cnWidthCell: NSLayoutConstraint!
    @IBOutlet weak var cnHeightCell: NSLayoutConstraint!
    
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblNameMovie: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setPhoto(urlPhoto: String){
        if(urlPhoto != ""){
            let url = URL(string: urlPhoto)!
            let placeholderImage = UIImage(named: "movie_placeholder")!
            self.imgMovie.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: .crossDissolve(0.2))
        }
    }
    
    func updateSizeCell(widthCollection: CGFloat, heightCollection: CGFloat) {
        self.cnWidthCell.constant = widthCollection
        self.cnHeightCell.constant = heightCollection
    }
    
}
