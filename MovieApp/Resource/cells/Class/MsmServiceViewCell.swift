//
//  MsmServiceViewCell.swift
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

import UIKit

class MsmServiceViewCell: UICollectionViewCell {

    @IBOutlet weak var cnHeightCell: NSLayoutConstraint!
    @IBOutlet weak var cnWidthCell: NSLayoutConstraint!
    
    @IBOutlet weak var labelMsm: UILabel!
    @IBOutlet weak var btnTryAgain: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateSizeCell(widthCollection: CGFloat, heightCollection: CGFloat){
        self.cnWidthCell.constant = CGFloat(Int(widthCollection))
        self.cnHeightCell.constant = CGFloat(Int(heightCollection))
    }
    
    func cleanAllActionFromButton(){
        self.btnTryAgain.removeTarget(nil, action: nil, for: .allEvents)
    }
    
}
