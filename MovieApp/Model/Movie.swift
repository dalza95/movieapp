//
//  Movie.swift
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

import Foundation

class Movie {
    var id: Int!
    var name: String!
    var year: Int!
    var overview: String!
    var urlImage: String!
    
    init(id: Int!, name: String!, year: Int!, overview: String!, urlImage: String!) {
        
        self.id = id
        self.name = name
        self.year = year
        self.overview = overview
        self.urlImage = urlImage
        
    }
    
}
