//
//  GlobalMovieApp.swift
//  MovieApp
//
//  Created by Diego Alza on 11/07/19.
//  Copyright © 2019 dalza. All rights reserved.
//

import Foundation

class GlobalMovie {

    static var baseUrlMovieApi = "https://api.trakt.tv/"
    static var baseUrlImageApi = "https://api.themoviedb.org/3/movie/"
    
    static var api_secretKey = "cd36c8f1a439e976fe75a67f5497f15570af145e59148db1c5b311c77e68a03a"
    static var api_version = 2
    static var image_secretKey = "be3f6404b66193ca9e5f53a08572b9ae"
    
    static func getMovies(query:String, page: Int, limit: Int) -> String{
        return baseUrlMovieApi + "movies/popular?query=" + query + "&page=\(page)&limit=\(limit)&extended=full"
    }
    
    static func getUrlImageFromTheMovieDB (idImage: Int) -> String{
        return baseUrlImageApi + "\(idImage)?api_key=" + image_secretKey + "&language=en-US"
    }
    
    
}
